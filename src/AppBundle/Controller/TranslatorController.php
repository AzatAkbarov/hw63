<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Phrase;
use AppBundle\Entity\PhraseTranslation;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;

class TranslatorController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $placeholderForInput = 'enter a new phrase in Russian';
        $labelForSubmit = 'add a new phrase';

        $form = $this->createFormBuilder()
            ->add('ruContent', TextType::class, ['label'=> false, 'attr' => [
                'value' => null,
                'placeholder' => $placeholderForInput
            ]])
            ->add('save', SubmitType::class, ['label' => $labelForSubmit])
            ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid() && !empty($form->getData())) {

            $data = $form->getData();
            $phrase = new Phrase();

            /** @var  PhraseTranslation $ru_phrase */
            $ru_phrase = $phrase->translate('ru');
            $ru_phrase->setContent($data['ruContent']);

            $em = $this->getDoctrine()->getManager();
            $em->persist($phrase);

            $phrase->mergeNewTranslations();
            $em->flush();

            return $this->redirectToRoute('homepage');
        }

        $phrases = $this->getDoctrine()->getRepository('AppBundle:Phrase')->findAll();

        return $this->render('@App/Translator/index.html.twig', [
            'form' => $form->createView(),
            'phrases' => $phrases
        ]);

    }

    /**
     * @Route("/phrase/{id}/show_translates")
     * @param $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showTranslationsPhrases($id, Request $request)
    {

        $labelForSubmitTranslate = 'translation.form.add translation';
        $enPlaceholder = 'translation.phrase in english';
        $frPlaceholder = 'translation.phrase in french';
        $kzPlaceholder = 'translation.phrase in kazakh';
        $kgPlaceholder = 'translation.phrase in kyrgyz';
        $trPlaceholder = 'translation.phrase in turkish';

        $form = $this->createFormBuilder()
            ->add('enContent', TextType::class, ['label'=> false,'required' => false, 'attr' => ['placeholder' => $enPlaceholder] ])
            ->add('frContent', TextType::class, ['label'=> false,'required' => false, 'attr' => ['placeholder' => $frPlaceholder] ])
            ->add('kzContent', TextType::class, ['label'=> false,'required' => false, 'attr' => ['placeholder' => $kzPlaceholder] ])
            ->add('kgContent', TextType::class, ['label'=> false,'required' => false, 'attr' => ['placeholder' => $kgPlaceholder] ])
            ->add('trContent', TextType::class, ['label'=> false,'required' => false, 'attr' => ['placeholder' => $trPlaceholder] ])
            ->add('save', SubmitType::class, ['label' => $labelForSubmitTranslate ])
            ->getForm();

        $form->handleRequest($request);

        $phrase = $this->getDoctrine()->getRepository('AppBundle:Phrase')->find($id);

        if($form->isSubmitted()) {

            $data = $form->getData();

            if ($data['enContent'] !== null) {
                /** @var  PhraseTranslation $en_phrase */
                $en_phrase = $phrase->translate('en',false);
                $en_phrase->setContent($data['enContent']);
            }

            if ($data['frContent'] !== null) {
                /** @var  PhraseTranslation $fr_phrase */
                $fr_phrase = $phrase->translate('fr',false);
                $fr_phrase->setContent($data['frContent']);
            }

            if ($data['kzContent'] !== null) {
                /** @var  PhraseTranslation $kz_phrase */
                $kz_phrase = $phrase->translate('kz',false);
                $kz_phrase->setContent($data['kzContent']);
            }

            if ($data['kgContent'] !== null) {
                /** @var  PhraseTranslation $kg_phrase */
                $kg_phrase = $phrase->translate('kg',false);
                $kg_phrase->setContent($data['kgContent']);
            }

            if ($data['trContent'] !== null) {
                /** @var  PhraseTranslation $tr_phrase */
                $tr_phrase = $phrase->translate('tr',false);
                $tr_phrase->setContent($data['trContent']);
            }

            $em = $this->getDoctrine()->getManager();

            $em->persist($phrase);
            $phrase->mergeNewTranslations();
            $em->flush();

            return $this->redirect(
                $request
                    ->headers
                    ->get('referer')
            );

        }

        $arrayOfTranslation = [
            'id' => $phrase->getId(),
            'ruContent' => $phrase->translate('ru', false)->getContent() ?: false,
            'enContent' => $phrase->translate('en', false)->getContent() ?: false,
            'frContent' => $phrase->translate('fr', false)->getContent() ?: false,
            'kzContent' => $phrase->translate('kz', false)->getContent() ?: false,
            'kgContent' => $phrase->translate('kg', false)->getContent() ?: false,
            'trContent' => $phrase->translate('tr', false)->getContent() ?: false,
        ];

        return $this->render('@App/Translator/show_translate_phrase.html.twig', [
            'phrase' => $arrayOfTranslation,
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/{_locale}/change_locale", requirements = {"_locale" : "en|ru"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function changeLocaleAction(Request $request) {
        return $this->redirect(
            $request
                ->headers
                ->get('referer')
        );
    }
}