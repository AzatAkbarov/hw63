<?php

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\Phrase;
use AppBundle\Entity\PhraseTranslation;
use Doctrine\Bundle\FixturesBundle\ORMFixtureInterface;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadPhraseData implements FixtureInterface, ORMFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $phrase1 = new Phrase();

        /** @var  PhraseTranslation $ru_phrase1 */
        $ru_phrase1 = $phrase1->translate('ru');
        $ru_phrase1->setContent('что то на русском');

        /** @var  PhraseTranslation $en_phrase1 */
        $en_phrase1 = $phrase1->translate('en');
        $en_phrase1->setContent('something in english');

        /** @var  PhraseTranslation $fr_phrase1 */
        $fr_phrase1 = $phrase1->translate('fr');
        $fr_phrase1->setContent('quelque chose en français');

        /** @var  PhraseTranslation $kaz_phrase1 */
        $kaz_phrase1 = $phrase1->translate('kz');
        $kaz_phrase1->setContent('қазақ тілінде');

        /** @var  PhraseTranslation $kg_phrase1 */
        $kg_phrase1 = $phrase1->translate('kg');
        $kg_phrase1->setContent('бирдеме кыргызча');

        /** @var  PhraseTranslation $tr_phrase1 */
        $tr_phrase1 = $phrase1->translate('tr');
        $tr_phrase1->setContent('türkçe bir şey');

        $manager->persist($phrase1);
        $phrase1->mergeNewTranslations();




        $phrase2 = new Phrase();

        /** @var  PhraseTranslation $ru_phrase2 */
        $ru_phrase2 = $phrase2->translate('ru');
        $ru_phrase2->setContent('еще что то на русском');

        /** @var  PhraseTranslation $en_phrase2 */
        $en_phrase2 = $phrase2->translate('en');
        $en_phrase2->setContent('still something in English');

        /** @var  PhraseTranslation $fr_phrase2 */
        $fr_phrase2 = $phrase2->translate('fr');
        $fr_phrase2->setContent('encore quelque chose en français');

        /** @var  PhraseTranslation $kaz_phrase2 */
        $kaz_phrase2 = $phrase2->translate('kz');
        $kaz_phrase2->setContent('қазақ тілінде әлі де бір нәрсе');

        $manager->persist($phrase2);
        $phrase2->mergeNewTranslations();



        $phrase3 = new Phrase();

        /** @var  PhraseTranslation $ru_phrase3 */
        $ru_phrase3 = $phrase3->translate('ru');
        $ru_phrase3->setContent('все еще что то на русском');

        $manager->persist($phrase3);
        $phrase3->mergeNewTranslations();

        $manager->flush();
    }
}