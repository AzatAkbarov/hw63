<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\User;
use Doctrine\Bundle\FixturesBundle\ORMFixtureInterface;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadUserData implements FixtureInterface, ORMFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $user1 = new User();
        $password = password_hash('qwe',PASSWORD_DEFAULT);
        $user1
            ->setUsername('qwe')
            ->setEmail('qwe@qwe.qwe')
            ->setEnabled(1)
            ->setPassword($password)
            ->setRoles(['ROLE_USER']);

        $manager->persist($user1);

        $user2 = new User();
        $password = password_hash('asd',PASSWORD_DEFAULT);
        $user2
            ->setUsername('asd')
            ->setEmail('asd@asd.asd')
            ->setEnabled(1)
            ->setPassword($password)
            ->setRoles(['ROLE_USER']);

        $manager->persist($user2);

        $manager->flush();
    }
}
