<?php

namespace AppBundle\Features\Context;

use Behat\Gherkin\Node\TableNode;
use Behat\MinkExtension\Context\MinkContext;
use Behat\Symfony2Extension\Context\KernelAwareContext;
use Symfony\Component\HttpKernel\KernelInterface;


/**
 * Defines application features from the specific context.
 */
class FeatureContext extends MinkContext implements KernelAwareContext
{
    /** @var  KernelInterface */
    private $kernel;

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
    }

    /**
     * @When /^я вижу слово "([^"]*)" где\-то на странице$/
     */
    public function яВижуСловоГдеТоНаСтранице($arg1)
    {
        $this->assertPageContainsText($arg1);
    }

    protected function getContainer()
    {
        return $this->kernel->getContainer();
    }

    /**
     * @When /^я нахожусь на странице "([^"]*)"$/
     */
    public function яНахожусьНаГлавнойСтранице($routeName)
    {
        $this->visit($this->getContainer()->get('router')->generate($routeName));
    }

    /**
     * @When /^я вижу ссылку "([^"]*)" и нажимаю на нее$/
     */
    public function яВижуСсылкуИНажимаюНаНее($link) {
        $this->clickLink($link);
    }

    /**
     * @When /^я вижу форму авторизации и ввожу данные в поля "([^"]*)" и "([^"]*)",потом кликаю на кнопку "([^"]*)"$/
     */
    public function яВижуФормуАвторизацииИАвторизуюсь($field1,$field2,$button) {
        $this->fillField($field1, "qwe");
        $this->fillField($field2, "qwe");
        $this->pressButton($button);
    }


    /**
     * @When /^я ввожу новые слова на русском в поле "([^"]*)" и нажимаю кнопку "([^"]*)"$/
     */
    public function яВвожуНовыеСловаНаРусском($field1,$button, TableNode $tableNode) {
        foreach ($tableNode as $phrase) {
            $this->fillField($field1, $phrase['phrase']);
            $this->pressButton($button);
        }
    }


    /**
     * @When /^я ищу на странице слово-ссылку, перехожу на страницу переводов и ввожу переводы$/
     */
    public function ищуСловоИПерехожуНаСтраницуПереводовИВвожуПереводы(TableNode $tableNode) {
        foreach ($tableNode as $phrase) {
            $this->clickLink($phrase['phrase']);
            $this->fillField('Фраза на английском', 'Перевод на английский');
            $this->fillField('Фраза на французском', 'Перевод на французский');
            $this->fillField('Фраза на казахском', 'Перевод на казахский');
            $this->fillField('Фраза на кыргызском', 'Перевод на кыргызский');
            $this->fillField('Фраза на турецком', 'Перевод на турецкий');
            $this->pressButton("Добавить перевод");
            $this->clickLink('На главную');
        }
    }

    /**
     * Sets Kernel instance.
     *
     * @param KernelInterface $kernel
     */
    public function setKernel(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }
}
